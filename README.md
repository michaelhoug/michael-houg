As a Plymouth mortgage lender, Michael Houg has specialized in helping people that are in the very early stages of the home buying process, since 1998. If you are looking for more information, give Michael a call today!

Address : 10425 49th Ave N, Plymouth, MN 55442

Phone : 612-310-7962